package test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import framework.algorithm.Localizer;
import framework.algorithm.Particle;
import framework.algorithm.ParticleFilterModel;
import framework.robots.IRobotMovement;
import framework.scanner.ILaserScanner;
import framework.scanner.MapPoint;

public class Test {

	public static void main(String[] args) throws Exception{
		long start = System.currentTimeMillis();
		int n = 10000;
		File roson = new File("./makieta w robolabie - obrys w metrach.dxf.roson");
		ParticleFilterModel model = new ParticleFilterModel(roson);
		
		ArrayList<Particle> particles = new ArrayList<Particle>(n);
		for (int i=0; i < n; i++) {
			particles.add(model.randParticle(1.0));
		}
		long stop = System.currentTimeMillis();
		System.out.println((double)(stop-start) / n);
		
		LocalizerLogFile logFile = new LocalizerLogFile(new File("./2014-01-30_16-13-003.txt"));
		
//		LocalizerLogFile logFile = new LocalizerLogFile(new File("log/testLog.txt"));
		IRobotMovement localLogRobotMovement = new LocalLogRobotMovement(logFile);
		
		ILaserScanner scanner = new LocalLogLaserScanner(logFile);
		Localizer localizer = new Localizer(roson, localLogRobotMovement, scanner);
		
		List<MapPoint> log = scanner.singleScan();
		start = System.currentTimeMillis();
		for (Particle p : particles) {
			localizer.calculateParticleAdjustment(p, log);
		}
		stop = System.currentTimeMillis();
		System.out.println((double)(stop - start) / particles.size());
		
		double x = 0;
		start = System.currentTimeMillis();
		for (Particle p : particles) {
			if (p == null) {
				System.out.println(p);
			}
			x = intersectiondouble(p.getX(), p.getY()+1, p.getX()+2, p.getY()+3, p.getX()+4, p.getY()+5, p.getX()+6, p.getY()+7);
		}
		stop= System.currentTimeMillis();
		System.out.println((double)(stop - start) / particles.size());
		System.out.println(x);
		
	}
	
	
	public static Double intersectiondouble(double x1,double y1,double
			x2,double y2, double x3, double y3, double x4,double y4)
			    {
			        double d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
			        if (d == 0) return null;

			            double xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
			            double yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;

			            return xi*yi;
			    }




}

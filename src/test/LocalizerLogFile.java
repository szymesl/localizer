package test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;


public class LocalizerLogFile {

	private List<ScanLog> logs;
	private int i;
	public LocalizerLogFile(File logFile) throws IOException {
		this.logs = ScanLog.readScanLogs(logFile);
		i=0;
	}
	
	public MotorsCurrentSpeed getCurrentMotorsSpeed() {
		ScanLog log = logs.get(i);
		MotorsCurrentSpeed mcs = log.getMotorCurrentSpeed();
		i++;
		return mcs;
	}

	public long getActualTime() {
		return logs.get(i).getTimestamp();
	}

	public ScanLog getActualScanLog() {
		return logs.get(i);
	}

	public boolean hasNextLog() {
		return i < logs.size()-1;
	}
}

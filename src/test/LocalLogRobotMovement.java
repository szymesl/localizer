package test;

import java.io.IOException;
import java.util.List;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;
import framework.algorithm.Particle;
import framework.robots.IRobotMovement;
import framework.robots.IRobotState;
import framework.robots.PandaRobotMovement;
import framework.robots.PandaRobotState;

public class LocalLogRobotMovement implements IRobotMovement {

	private LocalizerLogFile logFileKeeper;
	private PandaRobotMovement pandaRobotMovement;

	public LocalLogRobotMovement(LocalizerLogFile logFile) {
		this.logFileKeeper = logFile;
		try {
			this.pandaRobotMovement = new PandaRobotMovement(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void moveParticles(IRobotState previousMove, IRobotState currentMove, List<Particle> particles) {
		pandaRobotMovement.moveParticles(previousMove, currentMove, particles);
	}

	@Override
	public IRobotState getCurrentState() throws IOException, Exception {
		MotorsCurrentSpeed mcs = logFileKeeper.getCurrentMotorsSpeed();
		mcs.setAvailable();
		PandaRobotState pandaMove = new PandaRobotState(logFileKeeper.getActualTime(), mcs.getFrontLeftSpeed(), mcs.getFrontRightSpeed(), mcs.getRearLeftSpeed(),
				mcs.getRearRightSpeed());
		return pandaMove;
	}

}

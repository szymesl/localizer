package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;

import framework.scanner.MapPoint;

public class ScanLog {	
	
	private long timestamp;
	List<MapPoint> mapPoints;
	public List<MapPoint> getMapPoints() {
		return mapPoints;
	}

	public void setMeasures(List<MapPoint> mapPoints) {
		this.mapPoints = mapPoints;
	}

	private int FRspeed;
	private int FLspeed;
	private int RRspeed;
	private int RLspeed;
	public static List<ScanLog> readScanLogs(File logFile) throws IOException {

		BufferedReader reader;
		reader = new BufferedReader(new FileReader(logFile));
		String line = reader.readLine();
		String[] separated = line.split(" , ");
		List<ScanLog> toReturn = new ArrayList<ScanLog>();
		while ((line = reader.readLine()) != null) {
			ScanLog log = new ScanLog();
			separated = line.split(" , ");
			log.setTimestamp(Long.parseLong(separated[0]));
			log.setFRspeed(Integer.parseInt(separated[1]));
			log.setFLspeed(Integer.parseInt(separated[2]));
			log.setRRspeed(Integer.parseInt(separated[3]));
			log.setRLspeed(Integer.parseInt(separated[4]));
			

			List<MapPoint> measures = new ArrayList<MapPoint>();
			for (int i = 5; i+1 < separated.length; i = i + 2) {
				MapPoint measure = new MapPoint(Double.parseDouble(separated[i+1]), Double.parseDouble(separated[i]), log.getTimestamp());
				measures.add(measure);
			}
			log.setMeasures(measures);
			toReturn.add(log);
		}
		reader.close();
		return toReturn;
	}
	
	public MotorsCurrentSpeed getMotorCurrentSpeed() {
		return new MotorsCurrentSpeed(FLspeed, FRspeed, RLspeed, RRspeed);
	}

	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public void setFRspeed(int fRspeed) {
		FRspeed = fRspeed;
	}

	public void setFLspeed(int fLspeed) {
		FLspeed = fLspeed;
	}

	public void setRRspeed(int rRspeed) {
		RRspeed = rRspeed;
	}

	public void setRLspeed(int rLspeed) {
		RLspeed = rLspeed;
	}
}

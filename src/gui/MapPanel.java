package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import framework.algorithm.Particle;
import framework.algorithm.Position;
import framework.roson.Gate;
import framework.roson.RosonReader;
import framework.roson.Wall;

public class MapPanel extends JComponent {

	private List<Wall> walls;
	private List<Gate> gates;
	private List<Particle> particles = new ArrayList<Particle>();
	Particle[] pa = new Particle[250];
	private double minX, maxX, minY, maxY;
	private static final int margin = 5;
	private boolean particlesOccupied = false;
	private Position position;

	public MapPanel(List<Wall> walls) {
		super();
		setMap(walls);
		particles = new ArrayList<Particle>();
	}

	public void setGates(List<Gate> list) {
		this.gates = list;
	}

	public void clearParticles() {
		this.particles.clear();

	}

	public void setParticles(List<Particle> particles) {
		this.particles = particles;
		this.repaint();
	}

	public void setMap(List<Wall> walls, double minX, double maxX, double minY, double maxY) {
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		this.walls = walls;
		repaint();
	}

	public void setMap(List<Wall> walls2) {
		this.walls = walls2;
		double maxX = Double.MIN_VALUE;
		double minX = Double.MAX_VALUE;
		double maxY = Double.MIN_VALUE;
		double minY = Double.MAX_VALUE;
		for (Line2D line : walls2) {
			maxX = Math.max(maxX, line.getX1());
			maxX = Math.max(maxX, line.getX2());

			minX = Math.min(minX, line.getX1());
			minX = Math.min(minX, line.getX2());

			minY = Math.min(minY, line.getY1());
			minY = Math.min(minY, line.getY2());

			maxY = Math.max(maxY, line.getY1());
			maxY = Math.max(maxY, line.getY2());
		}

		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
		System.out.println("minMaxMaP:" + minX + " : " + minY + " : " + maxX + " : " + maxY);
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		Dimension d = this.getSize();
		double scale = Math.min((d.width - 2 * margin) / (maxX - minX), (d.height - 2 * margin) / (maxY - minY));
		
		for (Line2D wall : walls) {
			int x1 = modelX(wall.getX1(), scale);
			int x2 = modelX(wall.getX2(), scale);
			int y1 = (int) ((wall.getY1() - minY) * scale + margin);
			int y2 = (int) ((wall.getY2() - minY) * scale + margin);
			g.drawLine(x1, y1, x2, y2);
		}

		g.setColor(Color.RED);
		if (gates != null) {
			for (Gate gate : gates) {
				if (gate.isBlocked()) {
					int x1 = modelX(gate.getX1(), scale);
					int x2 = modelX(gate.getX2(), scale);
					int y1 = modelY(gate.getY1(), scale);
					int y2 = modelY(gate.getY2(), scale);
					g.drawLine(x1, y1, x2, y2);
				}
			}
		}

		g.setColor(Color.BLACK);
		Particle p;
		int pi = 0;
		double ax = 0.0;
		double ay = 0.0;

		double maxWeight = Double.MIN_VALUE;
		for (pi = 0; pi < particles.size(); pi++) {
			maxWeight = Math.max(maxWeight, particles.get(pi).getWeight());
		}
		maxWeight = Math.max(maxWeight, 0.5);
		for (pi = 0; pi < particles.size(); pi++) {
			p = particles.get(pi);
			int x = (int) ((p.getX() - minX) * scale + margin);
			int y = (int) ((p.getY() - minY) * scale + margin);
			ax += x;
			ay += y;
			g.setColor(Color.getHSBColor(0.0f, 1.0f, (float) p.getWeight() * 10.0f));
			g.drawOval(x, y, 4, 4);
			g.drawLine(x + 2, y + 2, x + 2 + (int) (7 * Math.cos(p.getAngle())),
					y + 2 + (int) (7 * Math.sin(p.getAngle())));
		}
		
		g.setColor(Color.GREEN);
		if (position != null) {
			int x = (int) ((position.getX() - minX) * scale + margin);
			int y = (int) ((position.getY() - minY) * scale + margin);
			g.drawOval(x, y, 5, 5);
		}
	}
	
	private int modelX(double mapX, double scale) {
		return (int) ((mapX - minX) * scale + margin);
	}
	
	private int modelY(double mapY, double scale) {
		return (int) ((mapY - minY) * scale + margin);
	}

	public void setPosition(Position p) {
		this.position = p;
	}
	

}

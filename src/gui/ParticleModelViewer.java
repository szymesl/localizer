package gui;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import framework.algorithm.Particle;
import framework.algorithm.ParticleModelObserver;
import framework.algorithm.Position;
import framework.roson.Wall;
import framework.scanner.MapPoint;

public class ParticleModelViewer implements ParticleModelObserver {

	private LaserMeasuresPanel laserMeasuresPanel;
	private MapPanel mapPanel;


	public ParticleModelViewer(List<Wall> walls) {
		laserMeasuresPanel = new LaserMeasuresPanel();
		mapPanel = new MapPanel(walls);
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame();
				JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
				splitPane.add(mapPanel);
				splitPane.add(laserMeasuresPanel);
				splitPane.setDividerLocation(500);
				frame.add(splitPane);
				frame.setSize(900, 600);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
				frame.repaint();				
			}
		});
	}

	@Override
	public void actualScanChange(List<MapPoint> list){

		double[] measures = new double[list.size()];
		double[] angles = new double[list.size()];

		int i = 0;
		for (MapPoint m : list) {
			angles[i] = m.getAngle();
			measures[i] = m.getDistance();
			i++;
		}
		laserMeasuresPanel.paintLaserMeasures(measures, angles);
	}

	@Override
	public void particleChange(List<Particle> particles) {
		mapPanel.setParticles(particles);
	}

	@Override
	public void mapChanged(List<Wall> walls) {
		mapPanel.setMap(walls);
	}

	@Override
	public void positionChanged(Position p) {
		mapPanel.setPosition(p);
		
	}

}

package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import test.LocalizerLogFile;
import framework.scanner.MapPoint;

public class LaserMeasuresPanel extends JComponent {
			
	static LaserMeasuresPanel lmp;
	private double[] measures;
	private double[] angles;

	public static double startAlfa = 270;
	public double scaleFactor = 0.15;
	
	public void paintLaserMeasures(double[] measures, double[] angles) {
		if (measures.length != angles.length) {
			System.out.println("Warning: arrays length not coherent");
		}
		this.measures = measures;
		this.angles = angles;
		this.repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Dimension size = this.getSize();
		int min = Math.min(size.height, size.width);
		double scale = scaleFactor * min;
		int centerX = size.width / 2;
		int centerY = size.height / 3 * 2;
		if (measures != null && angles != null)
			for (int i = 0; i < measures.length; i++) {
				g.setColor(Color.BLUE);
				int endX = (int) (centerX + Math.cos((startAlfa + angles[i])) * measures[i] * scale);
				int endY = (int) (centerY + Math.sin((startAlfa + angles[i])) * measures[i] * scale);
				g.drawLine(centerX, centerY, endX, endY);
				g.setColor(Color.RED);
				g.drawOval(endX, endY, 3, 3);
			}
	}
}

package framework.scanner;

import java.io.IOException;
import java.util.List;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

public class HokuyoLaserScanner implements ILaserScanner {

	private SCIP scip;
	private double maxLaserRange = 5.6;
	public HokuyoLaserScanner () {
		Serial.getAvailablePorts();
		String port = System.getProperties().getProperty("hokuyoPortName");
		scip = null;
		SerialPort serialPort;
		try {
			serialPort = SerialPortHelper.getHokuyoSerialPort(port);
			scip = new SCIP(serialPort.getInputStream(), serialPort.getOutputStream());
		} catch (PortInUseException | IOException | NoSuchPortException | UnsupportedCommOperationException e) {
			e.printStackTrace();
		}
		scip.laserOn();
	}
	
	@Override
	public List<MapPoint> singleScan() {
		List<MapPoint> mapPointList = scip.singleScan();
		for (MapPoint mp : mapPointList) {
			mp.setAngle(Math.toRadians(mp.getAngle()));
			mp.setDistance(mp.getDistance() / 1000);
		}
		return mapPointList;
	}

	@Override
	public double getMaxLaserRange() {
		return maxLaserRange ;
	}

}

package framework.scanner;
//package info.suder.navi.main.eye.hokuyo;

/**
 * @author GreenWing
 * @author Pawel Suder
 */
public class MapPoint {

    private double angle, distance;

    private long timeStamp;

    public MapPoint(double d, double a, long t) {
        distance = d;
        angle = a;
        timeStamp = t;
    }
    
    public void setDistance(double distance) {
    	this.distance = distance;
    }
    
    public void setAngle(double angle) {
    	this.angle = angle;
    }
    
    public void setTimeStamp (long timeStamp) {
    	this.timeStamp = timeStamp;
    }

    public double getDistance() {
        return distance;
    }

    public double getAngle() {
        return angle;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public double xValue() {
        return distance * Math.cos(Math.toRadians(angle));
    }

    public double yValue() {
        return distance * Math.sin(Math.toRadians(angle));
    }

    @Override
    public String toString() {
        return "Distance: " + distance + ", angle: " + angle + ", timestamp: " + timeStamp + "\n";
    }
}

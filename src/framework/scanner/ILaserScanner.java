package framework.scanner;

import java.util.List;

public interface ILaserScanner {

	public List<MapPoint> singleScan();
	public double getMaxLaserRange();
}

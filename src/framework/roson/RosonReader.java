package framework.roson;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

public class RosonReader {

	private JsonObject rosonObject;

	public RosonReader(File roson) throws FileNotFoundException {
		this.rosonObject = Json.createReader(new FileInputStream(roson)).readObject();
	}

	public List<Wall> readWalls() {
		JsonArray gates = rosonObject.getJsonArray("walls");
		List<Wall> walls;
		walls = new ArrayList<Wall>();
		double max = 0.0;
		for (JsonObject result : gates.getValuesAs(JsonObject.class)) {
			JsonObject from = result.getJsonObject("from");
			JsonObject to = result.getJsonObject("to");
			Point2D fromPoint = new Point2D.Double(from.getJsonNumber("x").doubleValue(), from.getJsonNumber("y").doubleValue());
			Point2D toPoint = new Point2D.Double(to.getJsonNumber("x").doubleValue(), to.getJsonNumber("y").doubleValue());
			
			max = Math.max(fromPoint.getX(), max);
			max = Math.max(toPoint.getX(), max);
			String id = result.getString("id");
			Wall wall = new Wall(fromPoint, toPoint, id);
			walls.add(wall);
		}
		System.out.println("MAAAAAAAAx: " + max);
		return walls;
	}

	public List<Space> readSpacesWithWalls(List<Wall> walls, List<Gate> gates) {
		List<Space> spacesList = new ArrayList<Space>();
		JsonArray jsonSpaces = rosonObject.getJsonArray("spaces");
		
		if (jsonSpaces == null) {
			Space s = new Space(0, 0, "null");
			s.setWalls(walls);
			s.setGates(gates);
			spacesList.add(s);
			return spacesList;
		}
		
		Map <String, List<Wall>>  wallsToSpace = new HashMap<String, List<Wall>>();
		Map <String, List<Gate>>  gatesToSpace = new HashMap<String, List<Gate>>();
		for (JsonObject jsonSpace : jsonSpaces.getValuesAs(JsonObject.class)) {
			double area = jsonSpace.getJsonNumber("area").doubleValue();
			double diameter = jsonSpace.getJsonNumber("diameter").doubleValue();
			String id = jsonSpace.getString("id");
			Space newSpace = new Space(area, diameter, id);
			spacesList.add(newSpace);
			wallsToSpace.put(id, new ArrayList<Wall>());
			gatesToSpace.put(id,new ArrayList<Gate>());
			
		}
		
		JsonArray jsonWallsToSpace = rosonObject.getJsonArray("space-walls");
		for (JsonObject sw : jsonWallsToSpace.getValuesAs(JsonObject.class)) {
			if (sw.getString("type").equals("space-wall")) {
				String wallId = sw.getString("wallId");
				String spaceId = sw.getString("spaceId");
				for (Wall w : walls) {
					if (w.getId().equals(wallId)) wallsToSpace.get(spaceId).add(w);
				}
			}
		}
		
		JsonArray jsonGatesToSpace = rosonObject.getJsonArray("space-gates");
		for (JsonObject sw : jsonGatesToSpace.getValuesAs(JsonObject.class)) {
			if (sw.getString("type").equals("space-gate")) {
				String gateId = sw.getString("gateId");
				String spaceId = sw.getString("spaceId");
				for (Gate g : gates) {
					if (g.getId().equals(gateId)) gatesToSpace.get(spaceId).add(g);
				}
			}
		}
		
		for (Space space : spacesList) {
			space.setWalls(wallsToSpace.get(space.getId()));
			space.setGates(gatesToSpace.get(space.getId()));
		}
		
		return spacesList;
	}

	public List<Gate> readgates() {
		JsonArray gates = rosonObject.getJsonArray("gates");
		List<Gate> toReturn;
		toReturn = new ArrayList<Gate>();
		for (JsonObject result : gates.getValuesAs(JsonObject.class)) {
			JsonObject from = result.getJsonObject("from");
			JsonObject to = result.getJsonObject("to");
			Point2D fromPoint = new Point2D.Double(from.getJsonNumber("x").doubleValue(), from.getJsonNumber("y").doubleValue());
			Point2D toPoint = new Point2D.Double(to.getJsonNumber("x").doubleValue(), to.getJsonNumber("y").doubleValue());
			String id = result.getString("id");
			boolean blocked = result.getJsonNumber("blocked").doubleValue() == 0.0;
			Gate wall = new Gate(fromPoint, toPoint, id, blocked);
			toReturn.add(wall);
		}
		return toReturn;
	}
}

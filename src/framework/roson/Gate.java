package framework.roson;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class Gate extends Line2D.Double{

	private String id;
	private boolean blocked;

	public Gate(Point2D fromPoint, Point2D toPoint, String id, boolean blocked) {
		super(fromPoint, toPoint);
		this.setId(id);
		this.setBlocked(blocked);
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isBlocked() {
		return blocked;
	}


}

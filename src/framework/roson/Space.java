package framework.roson;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

public class Space {
	private double area;
	private double diameter;
	private String id;
	private List<Wall> walls;
	private List<Gate> gates;

	private double minX;
	private double maxX;
	private double minY;
	private double maxY;

	public double getMinX() {
		return minX;
	}

	public void setMinX(double minX) {
		this.minX = minX;
	}

	public double getMaxX() {
		return maxX;
	}

	public void setMaxX(double maxX) {
		this.maxX = maxX;
	}

	public double getMinY() {
		return minY;
	}

	public void setMinY(double minY) {
		this.minY = minY;
	}

	public double getMaxY() {
		return maxY;
	}

	public void setMaxY(double maxY) {
		this.maxY = maxY;
	}

	public Space(double area, double diameter, String id) {
		super();
		walls = new ArrayList<Wall>();
		gates = new ArrayList<Gate>();
		this.area = area;
		this.diameter = diameter;
		this.id = id;
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Wall> getWalls() {
		return walls;
	}

	public void setWalls(List<Wall> walls) {
		this.minX = Double.MAX_VALUE;
		this.minY = Double.MAX_VALUE;
		this.maxX = Double.MIN_VALUE;
		this.maxY = Double.MIN_VALUE;

		for (Wall w : walls) {
			minX = Math.min(Math.min(w.getX1(), w.getX2()), minX);
			maxX = Math.max(Math.max(w.getX1(), w.getX2()), maxX);
			minY = Math.min(Math.min(w.getY1(), w.getY2()), minY);
			maxY = Math.max(Math.max(w.getY1(), w.getY2()), maxY);
		}

		this.walls = walls;
	}

	public List<Gate> getGates() {
		return gates;
	}

	public void setGates(List<Gate> gates) {
		this.gates = gates;
	}

	public boolean pointInsideBB(Point2D point) {
		
		if (point.getX() < getMinX() || point.getX() > getMaxX() || point.getY() < getMinY() || point.getY() > getMaxY()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public boolean pointInside(Point2D point) {
		if (!pointInsideBB(point)) {
			return false;
		}
		
		double LocalminX = minX - 5.0;
		double LocalminY = minY - 5.0;
		Line2D line = new Line2D.Double(new Point2D.Double(LocalminX, LocalminY), point);
		int countIntersections = 0;
		for (Line2D wall : getWalls()) {
			if (line.intersectsLine(wall))
				countIntersections++;
		}
		for (Line2D gate : getGates()) {
			if (line.intersectsLine(gate))
				countIntersections++;
		}
		return ((countIntersections & 1) == 1);
	}
	

	
}

package framework.roson;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class Wall extends Line2D.Double {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2712011054209653515L;
	private String id;

	public Wall(double x1, double y1, double x2, double y2, String id) {
		super(x1, y1, x2, y2);
		this.id = id;
	}
	
	public Wall(Point2D from, Point2D to, String id) {
		super(from, to);
		this.id = id;
	}
	
	public String getId() {
		return this.id;
	}
}

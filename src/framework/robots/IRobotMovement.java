package framework.robots;

import java.io.IOException;
import java.util.List;

import framework.algorithm.Particle;

public interface IRobotMovement {
	
	public void moveParticles(IRobotState previousMove, IRobotState currentMove, List<Particle> particle);
	
//	public void rotation(IRobotState previousMove, IRobotState currentMove, Particle particle);
	
	public IRobotState getCurrentState() throws IOException, Exception;

}

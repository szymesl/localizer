package framework.robots;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.edu.agh.amber.roboclaw.MotorsCurrentSpeed;
import pl.edu.agh.amber.roboclaw.RoboclawProxy;
import framework.algorithm.Particle;

public class PandaRobotMovement implements IRobotMovement {

	private static Random random = new Random();
	private final static double robotWidth = 0.3; // TODO przechowywa� gdzie�
													// informacje o robocie i
													// stamtad to brac

	private final RoboclawProxy roboclawProxy;
	private double translationDeviationFactor = 0.00005;
	private double angleDeviationFactor = 0.1;
	private double speedFactor = 0.1;

	public PandaRobotMovement(RoboclawProxy roboclawProxy) {
		this.roboclawProxy = roboclawProxy;
	}

	@Override
	public void moveParticles(IRobotState previousMove, IRobotState currentMove, List<Particle> particles) {

		PandaRobotState pandaPrevMove = (PandaRobotState) previousMove;
		PandaRobotState pandaCurrMove = (PandaRobotState) currentMove;

		double deltaTime = pandaCurrMove.getCurrentTime() - pandaPrevMove.getCurrentTime();
		double leftSpeed = pandaCurrMove.getlFrontWheelSpeed() + pandaCurrMove.getlRearWheelSpeed() + pandaPrevMove.getlFrontWheelSpeed()
				+ pandaPrevMove.getlRearWheelSpeed();
		double rightSpeed = pandaCurrMove.getrFrontWheelSpeed() + pandaCurrMove.getrRearWheelSpeed() + pandaPrevMove.getrFrontWheelSpeed()
				+ pandaPrevMove.getrRearWheelSpeed();

		
		leftSpeed *= speedFactor;
		rightSpeed *= speedFactor;
		
		double forw;
		double dev;
		double alfa;

		if (leftSpeed == rightSpeed) {
			dev = 0.0;
			forw = leftSpeed * deltaTime / 1000000;
			alfa = 0.0;
		} else {
			double r;

			r = robotWidth * (rightSpeed / (rightSpeed - leftSpeed) - 0.5);

			if (Math.abs(leftSpeed) > Math.abs(rightSpeed)) {
				alfa = -deltaTime / 1000000 * leftSpeed /  ((Math.abs(r) + 0.5)* robotWidth);
			} else {
				alfa = deltaTime / 1000000 * rightSpeed / ((Math.abs(r) + 0.5) * robotWidth);
			}

			alfa *= 0.8;
			dev = (-Math.cos(alfa) + 1) * r;
			forw = Math.sin(alfa) * r;

		}

		for (Particle particle : particles) {
			particle.setX(particle.getX() + Math.cos(particle.getAngle()) * forw + Math.sin(particle.getAngle() + Math.PI / 2) * dev + random.nextGaussian() * translationDeviationFactor  * (leftSpeed+rightSpeed));
			particle.setY(particle.getY() + Math.sin(particle.getAngle()) * forw + Math.cos(particle.getAngle() + Math.PI / 2) * dev + random.nextGaussian()* translationDeviationFactor * (leftSpeed+rightSpeed));
			particle.setAngle(particle.getAngle() + alfa + random.nextGaussian()/10);
		}
	}

	@Override
	public IRobotState getCurrentState() throws IOException, Exception {
		MotorsCurrentSpeed motorsCurrentSpeed = this.roboclawProxy.getCurrentMotorsSpeed();

		PandaRobotState pandaMove = new PandaRobotState(System.currentTimeMillis(), motorsCurrentSpeed.getFrontLeftSpeed(),
				motorsCurrentSpeed.getFrontRightSpeed(), motorsCurrentSpeed.getRearLeftSpeed(), motorsCurrentSpeed.getRearRightSpeed());

		return pandaMove;
	}
}

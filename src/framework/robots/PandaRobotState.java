package framework.robots;

public class PandaRobotState implements IRobotState {

	private final long currentTime;
	private final int lFrontWheelSpeed;
	private final int rFrontWheelSpeed;
	private final int lRearWheelSpeed;
	private final int rRearWheelSpeed;
	
	public PandaRobotState(long currentTime, int lFrontWheelSpeed,
			int rFrontWheelSpeed, int lRearWheelSpeed, int rRearWheelSpeed) {
		super();
		this.currentTime = currentTime;
		this.lFrontWheelSpeed = lFrontWheelSpeed;
		this.rFrontWheelSpeed = rFrontWheelSpeed;
		this.lRearWheelSpeed = lRearWheelSpeed;
		this.rRearWheelSpeed = rRearWheelSpeed;
	}

	public int getlFrontWheelSpeed() {
		return this.lFrontWheelSpeed;
	}

	public int getrFrontWheelSpeed() {
		return this.rFrontWheelSpeed;
	}

	public int getlRearWheelSpeed() {
		return this.lRearWheelSpeed;
	}

	public int getrRearWheelSpeed() {
		return this.rRearWheelSpeed;
	}

	public long getCurrentTime() {
		return this.currentTime;
	}
	
}

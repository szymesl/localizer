package framework.algorithm;


public class Position {

	private double x;
	private double y;
	private double angle;
	private double adjustment;
	private long timestamp;

	
	public Position(double x, double y, double angle, double adjustment, long timestamp) {
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.adjustment = adjustment;
		this.timestamp = timestamp;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAngle() {
		return angle;
	}

	public double getAdjustment() {
		return adjustment;
	}

	public long getTimestamp() {
		return timestamp;
	}

}

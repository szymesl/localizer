package framework.algorithm;

import java.awt.geom.Point2D;

public class Particle  extends Point2D.Double{

	private static final long serialVersionUID = -7244322489782015389L;
	private double angle;
	private double weight;


	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getAngle() {
		return angle;
	}
	
	public void setAngle( double angle) {
		this.angle = angle % 360;
	}

	public double getWeight() {
		return weight;
	}
	
	public void setWeight( double weight ) {
		this.weight = weight;
	}

	public Particle(double x, double y, double angle, double weight) {
		super();
		this.x = x;
		this.y = y;
		this.angle = angle;
		this.weight = weight;
	}
	
	public Particle copy(){
		return new Particle(x,y, angle, weight);
	}
	
	public String toString() {
		return getX() + " , " + getY() + " : " + getAngle() + " : " + getWeight();
	}

}

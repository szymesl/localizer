package framework.algorithm;

import framework.roson.Gate;
import framework.roson.RosonReader;
import framework.roson.Space;
import framework.roson.Wall;
import framework.scanner.MapPoint;
import gui.MapPanel;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import test.ScanLog;

public class ParticleFilterModel {

	private List<Particle> particles;
	private List<Wall> walls;
	private List<Gate> gates;
	private List<Space> spaces;

	private List<MapPoint> actualScanLog;
	private List<ParticleModelObserver> observers;

	private double minX;
	private double minY;
	private double maxX;
	private double maxY;
	private Position lastPosition;
	
	public double getMinX() {
		return minX;
	}

	public double getMinY() {
		return minY;
	}

	public double getMaxX() {
		return maxX;
	}

	public double getMaxY() {
		return maxY;
	}
	
	public void setLastPosition(Position position ) {
		this.lastPosition = position;
		for (ParticleModelObserver o : observers) {
			o.positionChanged(position);
		}
	}
	
	public Position getLastPosition() {
		return lastPosition;
	}

	public ParticleFilterModel(File rosonFile) throws IOException {
		particles = new ArrayList<Particle>();
		observers = new ArrayList<ParticleModelObserver>();
		RosonReader rosonReader = new RosonReader(rosonFile);
		setWalls(rosonReader.readWalls());
		setGates(rosonReader.readgates());
		setSpaces(rosonReader.readSpacesWithWalls(getWalls(), getGates()));
		setMinMax();
	}

	public void addObserver(ParticleModelObserver observer) {
		observers.add(observer);
	}

	public void removeObserver(Object observer) {
		observers.remove(observer);
	}
	
	public double getMaxWeight() {
		double maxW = 0;
		for( Particle particle : particles ){
			maxW = Math.max(maxW, particle.getWeight());
		}
		return maxW;	
	}

	public List<Particle> getParticles() {
		return particles;
	}

	public synchronized void setParticles(List<Particle> particles) {
		this.particles = particles;
		double maxW = 0;
		for (Particle particle : particles) {
			maxW = Math.max(maxW, particle.getWeight());
		}
		for (ParticleModelObserver o : observers) {
			o.particleChange(particles);
		}

	}

	public List<Wall> getWalls() {
		return walls;
	}

	public void setWalls(List<Wall> walls) {
		this.walls = walls;
		for (ParticleModelObserver o : observers) {
			o.mapChanged(walls);
		}
	}

	public boolean addParticle(Particle e) {
		boolean toReturn = particles.add(e);
		if (toReturn) {
			for (ParticleModelObserver o : observers) {
				o.particleChange(particles);
			}
		}
		return toReturn;
	}

	public void clear() {
		particles.clear();
		for (ParticleModelObserver o : observers) {
			o.particleChange(particles);
		}
	}

	public boolean removeParticle(Object o) {
		return particles.remove(o);
	}

	public boolean removeAllParticles(Collection<?> c) {
		return particles.removeAll(c);
	}

	public List<MapPoint> getActualScanLog() {
		return actualScanLog;
	}

	public void setActualScanLog(List<MapPoint> list) {
		this.actualScanLog = list;
		for (ParticleModelObserver o : observers) {
			o.actualScanChange(list);
		}
	}

	public Particle randParticle(double weight) {
		Particle newParticle;
		Point2D point;
		do {
			point = new Point2D.Double((maxX - minX) * Math.random() + minX, (maxY - minY) * Math.random() + minY);
		} while (!this.pointInside(point));
		newParticle = new Particle(point.getX(), point.getY(), Math.random() * 360, weight);
		return newParticle;
	}

	public void normalizeParticles() {
		double weightSum = 0.0;
		for (Particle p : getParticles()) {
			weightSum += p.getWeight();
		}
		for (Particle p : getParticles()) {
			p.setWeight(p.getWeight() / weightSum);
		}
	}

	public void notifyParticleModification() {
		for (ParticleModelObserver o : observers) {
			o.particleChange(particles);
		}
	}

	public boolean pointInside(Point2D point) {

		for (Space s : spaces) {
			if (s.pointInside(point))
				return true;
		}
		return false;
	}

	private void setMinMax() {
		minX = Double.MAX_VALUE;
		minY = Double.MAX_VALUE;
		maxX = Double.MIN_VALUE;
		maxY = Double.MIN_VALUE;
		for (Line2D wall : getWalls()) {
			minX = Math.min(wall.getX1(), minX);
			minX = Math.min(wall.getX2(), minX);
			minY = Math.min(wall.getY1(), minY);
			minY = Math.min(wall.getY2(), minY);

			maxX = Math.max(Math.max(wall.getX1(), wall.getX2()), maxX);
			maxY = Math.max(Math.max(wall.getY1(), wall.getY2()), maxY);
		}
	}

	public List<Line2D> getRelevantWalls(Particle p) {
		List<Line2D> wallsAngGates = new ArrayList<Line2D>();

		for (Space s : spaces) {
			if (s.pointInsideBB(p)) {
				wallsAngGates.addAll(s.getWalls());
				for (Gate g : s.getGates()) {
					if (g.isBlocked()) {
						wallsAngGates.add(g);
					}
				}
			}
		}

		return wallsAngGates;
	}

	public List<Gate> getGates() {
		return gates;
	}

	public void setGates(List<Gate> gates) {
		this.gates = gates;
	}

	public List<Space> getSpaces() {
		return spaces;
	}

	public void setSpaces(List<Space> spaces) {
		this.spaces = spaces;
	}

}

package framework.algorithm;

import java.util.Comparator;


public class ParticleWeightComparator implements Comparator<Particle>{

	@Override
	public int compare(Particle arg0, Particle arg1) {
		Particle p1 = (Particle) arg0;
		Particle p2 = (Particle) arg1;
		if (p1.getWeight() > p2.getWeight())
			return -1;
		if (p1.getWeight() < p2.getWeight())
			return 1;

		return 0;
	}
}

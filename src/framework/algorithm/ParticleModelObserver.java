package framework.algorithm;

import java.awt.geom.Line2D;
import java.util.List;

import test.ScanLog;

import framework.roson.Wall;
import framework.scanner.MapPoint;

public interface ParticleModelObserver {

	public void actualScanChange(List<MapPoint> list);
	public void particleChange(List<Particle> particles);
	public void mapChanged(List<Wall> walls);
	public void positionChanged(Position p);
}
